use core::prelude::v1::{Copy, Clone};

pub const AF_UNIX: i32 = 1;
pub const AT_FDCWD: i32 = -2;
pub const EAI_SYSTEM: i32 = 11;
pub const PTHREAD_STACK_MIN: usize = 16384;
pub const SIGPIPE: i32 = 13;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct sockaddr_storage {
    pub ss_len: ::c_uchar,
    pub ss_family: ::sa_family_t,
    pub __ss_pad1: [::c_char; 6],
    pub __ss_align: i64,
    pub __ss_pad2: [::c_char; 112],
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct sockaddr_un {
    pub sun_len: u8,
    pub sun_family: ::sa_family_t,
    pub sun_path: [::c_char; 104],
}

pub fn WTERMSIG(status: ::c_int) -> ::c_int {
    status & 0x7f
}
pub fn WIFEXITED(status: ::c_int) -> bool {
    (status & 0x7f) == 0
}
pub fn WEXITSTATUS(status: ::c_int) -> ::c_int {
    (status >> 8) & 0xff
}

extern "C" {
    pub fn aligned_alloc(arg1: ::size_t, arg2: ::size_t) -> *mut ::c_void;
    pub fn linkat(
        __dirfd1: ::c_int,
        __path1: *const ::c_char,
        __dirfd2: ::c_int,
        __path2: *const ::c_char,
        __flags: ::c_int,
    ) -> ::c_int;
    pub fn pread(
        __fd: ::c_int,
        __buf: *mut ::c_void,
        __nbytes: ::size_t,
        __offset: ::off_t,
    ) -> ::ssize_t;
    pub fn pthread_condattr_setclock(
        __attr: *mut ::pthread_condattr_t,
        __clock_id: ::clockid_t,
    ) -> ::c_int;
    pub fn pthread_create(
        __pthread: *mut ::pthread_t,
        __attr: *const ::pthread_attr_t,
        __start_routine: unsafe extern "C" fn(
            arg1: *mut ::c_void,
        ) -> *mut ::c_void,
        __arg: *mut ::c_void,
    ) -> ::c_int;
}